/**
 * All the possible language settings that must be accounted for
 */
var LanguageSettings = {
    AUTO : "Auto",
    EN: "English",
    ES: "Español"
}

var LanguageSelector = {
    /**
     * Initialise the language selector
     */
    initialise: function(startingQuality) {
        // Events for language selector
        LanguageSelector.events.initialise();

        // Starting value
        if (typeof startingQuality == 'undefined') {
            startingQuality = LanguageSettings.AUTO;
        }
        LanguageSelector.setLanguage(startingQuality);
    },

    /**
     * Set language with one of the values from LanguageSettings
     *
     * @param value
     */
    setLanguage: function(value) {
        // TODO: Highlight the current language setting

        // $('#jsVolumeButtonPopoutBgSelectedVolume').height($('#jsVolumeButtonPopoutBg').height() * value);

        // TODO: Inform the VideoPlayerInterface of the change in language.
        // e.g. VideoPlayerInterface.actions.languageChange(value);

        // TODO: Display the selected language icon in the timeline
    },

    /**
     * Build the ID of the language button
     */
    getLanguageButtonID: function(language) {
        return "jsLanguageSelector" + language + "Button";
    },

    /**
     * Define the events
     */
    events: {
        /**
         * Link up the events and the event handlers
         */
        initialise: function() {
            $('#jsLanguageSelectorButton').click(LanguageSelector.events.languageButtonClickEventHandler);
            $('#jsLanguageSelectorContainer').hover(LanguageSelector.events.languageButtonHoverInEventHandler, LanguageSelector.events.languageButtonHoverOutEventHandler);
            $('#jsLangaugeButtonPopoutBg').click(LanguageSelector.events.languagePopoutClick);
        },

        /**
         * Switch between two language settings
         */
        languageButtonClickEventHandler: function(e) {
            // TODO: How should the language be changed?
            // Between Auto and Full HD?
        },

        /**
         * Display the language selector control when the user hovers over the language icon
         */
        languageButtonHoverInEventHandler: function(e) {
            var button = $('#jsLanguageSelectorButton');
            var container = $('#');
            var popout = $('#jsLanguageSelectorButtonPopout');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if (!popout.hasClass('open')) {
                    // Open language control
                    popout.addClass('open');
                }
            }
        },

        /**
         * Hide the language selector control when the user moves their mouse away from the language icon
         */
        languageButtonHoverOutEventHandler: function(e) {
            var button = $('#jsLanguageSelectorButton');
            var popout = $('#jsLanguageSelectorButtonPopout');
            if (popout.hasClass('open')) {
                // Close language control
                popout.removeClass('open');
            }
        }
    }
}
