<?php

// NOTE: Include any required files here

if (isset($_GET["dev"])) {
    // Load the local App class instead of the EOV App class
    require_once "classes/App.devclass.php";
}

// Build a query string for the embedded video
$playerQueryParams = array('_timeline' => false);

// If uid is set, create video authentication params
if (!empty($_REQUEST['uid'])) {
    require_once "classes/Authenticator.class.php";
    $authenticator = new Authenticator();

    $playerQueryParams['uid'] = $_REQUEST['uid'];
    $playerQueryParams['ts'] = time();
    $playerQueryParams['key'] = $authenticator->createAuthenticationString(
        $playerQueryParams['uid'],
        $playerQueryParams['ts']
    );
}

$queryString = "?" . http_build_query($playerQueryParams);
$iframeVideoLink = "/whitelabel-landing-page/project/videoPlayer.php{$queryString}";
//$iframeVideoLink = "https://preprod.rtcvid.net/ez_energy/videoPlayer.php{$queryString}"; // Working PURL: f19u5ap5mi132s

// If a base_url is not set in a config .ini file, we'll use this instead
$defaultBaseUrl = "http://whitelabel-landing.pb.com";

?><!DOCTYPE html>
<!--[if IE 7]><html class="fluidplayer ie ie7 lteie8 lteie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="fluidplayer ie ie8 lteie8" lang="en"><![endif]-->
<!--[if gt IE 8]><!--><html class="fluidplayer" lang="en"><!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="vp" name="viewport" content="width=767">

    <title><?php echo App::getSetting("page_title"); ?></title>

    <meta name="description" content="<?php echo App::getSetting("page_description"); ?>">
    <meta property="og:url" content="<?php echo App::getSetting("base_url", $defaultBaseUrl); ?>/" />
    <meta property="og:title" content="<?php echo App::getSetting("page_title"); ?>" />
    <meta property="og:description" content="<?php echo App::getSetting("page_description"); ?>" />
    <meta property="og:image" content="<?php echo App::getSetting("base_url", $defaultBaseUrl); ?>/dist/images/og_image.jpg" />
    <meta property="og:image:width" content="690" />
    <meta property="og:image:height" content="400" />
    <meta name="twitter:card" content="summary_large_image">

    <link href="dist/css/style.css" rel="stylesheet">
    <!--[if lt IE 8]>
        <link href="dist/css/bootstrap-ie7.css" rel="stylesheet">
    <![endif]-->

    <link rel="canonical" href="<?php echo App::getSetting("base_url", $defaultBaseUrl); ?>/" />

    <link rel="shortcut icon" href="dist/images/favicons/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-16.png" sizes="16x16">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-32.png" sizes="32x32">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-57.png" sizes="57x57">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-72.png" sizes="72x72">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-96.png" sizes="96x96">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-120.png" sizes="120x120">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-128.png" sizes="128x128">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-144.png" sizes="144x144">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-152.png" sizes="152x152">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-195.png" sizes="195x195">
    <link rel="shortcut icon" type="image/png" href="dist/images/favicons/favicon-228.png" sizes="228x228">

    <!-- IE Mobile (Windows Phone) fix -->
    <script>
        (function () {
            if ("-ms-user-select" in document.documentElement.style && navigator.userAgent.match(/IEMobile/)) {
                var msViewportStyle = document.createElement("style");
                msViewportStyle.appendChild(
                    document.createTextNode("@-ms-viewport{width:auto!important}")
                );
                document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
            }
        })();
    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <div class="full-wrapper">
        <div class="container container-box">
            <div class="row">
                <?php require_once "partials/header.php" ?>
            </div>

            <div class="row">
                <?php require_once "partials/sidebar-left.php" ?>
                <?php require_once "partials/player.php" ?>
                <?php require_once "partials/sidebar-right.php" ?>
            </div>

            <div class="row">
                <p class="prepared-for-text">Prepared for <span id="jsPerson"></span></p>
            </div>

            <div class="row">
                <?php require_once "partials/timeline.php" ?>
            </div>
        </div>

        <div class="container">
            <?php require_once "partials/footer.php" ?>
        </div>
    </div>

    <script src="dist/js/script.js"></script>

    <?php
    if (App::getSetting("google_analytics_enabled", false)) {
    ?>
    <!-- Remove or change this if you're using a different analytics provider -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', '<?php echo App::getSetting("google_analytics_id") ?>', 'auto');
        ga('send', 'pageview');
    </script>
    <?php
    }
    ?>

</body>
</html>