        <div class="footer row">
            <div class="col-xs-4 nopad text-left">
                &copy; <?php echo date('Y') ?> Pitney Bowes, Inc
            </div>

            <div class="col-xs-4 nopad text-center">
                <a href="http://www.pitneybowes.com/us/customer-engagement-marketing/synchronized-communications-execution/engageone-video-personalized-video.html" target="_blank">
                    Powered by Pitney Bowes <br>
                    <strong>EngageOne&reg; Video</strong>
                </a>
            </div>

            <div class="col-xs-4 nopad text-right">
                <a href="#" target="_blank">Help</a> |
                <a href="#" target="_blank">Privacy Policy</a>
            </div>
        </div>